Format: 3.0 (quilt)
Source: amigu
Binary: amigu
Architecture: any
Version: 0.7.6-2
Maintainer: Equipo de  Desarrollo de Canaima GNU/Linux <desarrolladores@canaima.softwarelibre.gob.ve>
Uploaders: Erick Birbe <erickcion@gmail.com>, Orlando Andrés Fiol Carballo <fiolorlando@gmail.com>, Luis Alejandro Martínez Faneyth <martinez.faneyth@gmail.com>
Homepage: http://canaima.softwarelibre.gob.ve/
Standards-Version: 3.9.1
Vcs-Browser: http://gitorious.org/canaima-gnu-linux/amigu-canaima/trees/master
Vcs-Git: git://gitorious.org/canaima-gnu-linux/amigu-canaima.git
Build-Depends: debhelper (>= 7.0.50~), python (>= 2.6), quilt
Package-List: 
 amigu deb utils extra
Checksums-Sha1: 
 f7bebe05e2c9812f86ce5832465d484b22af9b3a 1983246 amigu_0.7.6.orig.tar.gz
 ff2f33208590957ab83b760e58b6ca659f2b3824 50476 amigu_0.7.6-2.debian.tar.gz
Checksums-Sha256: 
 699d758aae77bdd7ee8c6ae07f4659362e5d79c070dfe68dc21752eebb1d9fc0 1983246 amigu_0.7.6.orig.tar.gz
 62e772359919f90e30d026010a5dbe057db27e46d0924623548d4c29f4584461 50476 amigu_0.7.6-2.debian.tar.gz
Files: 
 3551fbd9510d9aadc4bf8240bfecf2d9 1983246 amigu_0.7.6.orig.tar.gz
 abd42b169248a46ea5c61921c9bdaf69 50476 amigu_0.7.6-2.debian.tar.gz
