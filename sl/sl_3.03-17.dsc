Format: 3.0 (quilt)
Source: sl
Binary: sl
Architecture: any
Version: 3.03-17
Maintainer: Hiroyuki Yamamoto <yama1066@gmail.com>
Homepage: http://www.tkl.iis.u-tokyo.ac.jp/~toyoda/index_e.html
Standards-Version: 3.9.3
Build-Depends: cdbs, debhelper (>= 9), libncurses5-dev
Package-List: 
 sl deb games optional
Checksums-Sha1: 
 d0a8e52ef649cd3bbf02c099e9991dc7cb9b60c3 3776 sl_3.03.orig.tar.gz
 cb00197a4569003d271ef6e79b853146700f1177 16521 sl_3.03-17.debian.tar.gz
Checksums-Sha256: 
 5986d9d47ea5e812d0cbd54a0fc20f127a02d13b45469bb51ec63856a5a6d3aa 3776 sl_3.03.orig.tar.gz
 e432530cf74ca603db986ed9b412eae33730de44b26d7643b8f21ad828c88ee0 16521 sl_3.03-17.debian.tar.gz
Files: 
 d0d997b964bb3478f7f4968eee13c698 3776 sl_3.03.orig.tar.gz
 69846a4272314c761c193d0e282dab82 16521 sl_3.03-17.debian.tar.gz
